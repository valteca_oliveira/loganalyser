﻿using LogAnalyser.Domain.Interfaces.Strategy;
using System.Collections.Generic;
using System.Linq;
using LogAnalyser.Domain.Entities;

namespace LogAnalyser.Domain.Strategies
{
    public class AnalyserStrategy : IAnalyserStrategy
    {
        public IEnumerable<LogWebhookReport> GetTopUrl(IEnumerable<LogWebhook> logList, int top)
        {
            var logListGrouppedByUrl = logList.GroupBy(x => x.RequestToUrl);
            var sumarizedList = new List<LogWebhookReport>();
            logListGrouppedByUrl.ToList().ForEach(x => sumarizedList.Add(new LogWebhookReport { LogInfo = x.First().RequestToUrl, TotalCount = x.Count() }));
            return sumarizedList.OrderByDescending(x => x.TotalCount).Take(top);
        }

        public IEnumerable<LogWebhookReport> GetTopStatusCode(IEnumerable<LogWebhook> logList)
        {
            var logListGrouppedByStatusCode = logList.GroupBy(x => x.ResponseStatusCode);
            var sumarizedList = new List<LogWebhookReport>();
            logListGrouppedByStatusCode.ToList().ForEach(x => sumarizedList.Add(new LogWebhookReport { LogInfo = x.First().ResponseStatusCode, TotalCount = x.Count() }));
            return sumarizedList.OrderByDescending(x => x.TotalCount);
        }
    }
}
