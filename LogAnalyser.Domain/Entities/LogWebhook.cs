﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyser.Domain.Entities
{
    public class LogWebhook
    {
        public string RequestToUrl { get; set; }
        public string ResponseStatusCode { get; set; }
    }
}
