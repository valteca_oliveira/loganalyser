﻿namespace LogAnalyser.Domain.Entities
{
    public class LogWebhookReport
    {
        public string LogInfo { get; set; }
        public int TotalCount { get; set; }
    }
}
