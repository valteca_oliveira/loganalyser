﻿using LogAnalyser.Domain.Entities;
using LogAnalyser.Domain.Interfaces.Repository;
using LogAnalyser.Domain.Interfaces.Strategy;
using LogAnalyser.Domain.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LogAnalyser.Domain
{
    public class Analyser
    {
        IAnalyserStrategy _strategy;
        ILogWebhookRepository _repository;

        public Analyser(ILogWebhookRepository repository, IAnalyserStrategy strategy)
        {
            _repository = repository;
            _strategy = strategy;
        }

        public string GetReport(string filePath)
        {
            StringBuilder strReport = new StringBuilder();
            
            try
            {
                int countTopReportUrl = 0;
                if (!Int32.TryParse(ConfigurationWrapper.GetConfiguration("COUNT_TOP_URL_REPORT"), out countTopReportUrl))
                    throw new Exception("Erro na conversão da configuração 'COUNT_TOP_URL_REPORT' para número");

                var logList = _repository.Load(filePath);

                strReport.AppendLine();
                var logListTopUrl = _strategy.GetTopUrl(logList, countTopReportUrl);
                strReport.AppendLine(GenerateReport(logListTopUrl, "URLs"));

                var logListTopStatusCode = _strategy.GetTopStatusCode(logList);
                strReport.AppendLine(GenerateReport(logListTopStatusCode, "STATUS CODE"));

                strReport.AppendLine(String.Format("TOTAL DE LINHAS VALIDAS: {0}", logList.Count));
            }
            catch (FileNotFoundException ex)
            {
                strReport.Append(String.Format("ERRO: {0}\n\nFavor validar o path informado na chave de configuração 'LOG_FILE_PATH', contida no arquivo App.config", ex.Message));
            }
            catch (Exception ex)
            {
                strReport.Append(String.Format("ERRO: {0}", ex.Message));                
            }

            return strReport.ToString();
        }
        
        private string GenerateReport(IEnumerable<LogWebhookReport> logWebhookReportList, string infoReport)
        {
            var _report = new StringBuilder();
            _report.AppendLine("========================");
            _report.AppendLine(String.Format("RELATÓRIO DE {0}", infoReport));
            _report.AppendLine("========================");
            logWebhookReportList.ToList().ForEach(x => _report.AppendLine(String.Format("{0} - {1}", x.LogInfo, x.TotalCount)));
            return _report.ToString();            
        }        
    }
}
