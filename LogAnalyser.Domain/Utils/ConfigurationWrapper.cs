﻿using System.Configuration;

namespace LogAnalyser.Domain.Utils
{
    public static class ConfigurationWrapper
    {
        public static string GetConfiguration(string appKey)
        {
            return ConfigurationManager.AppSettings[appKey];
        }
    }
}
