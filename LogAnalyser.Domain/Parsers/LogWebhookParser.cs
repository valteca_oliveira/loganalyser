﻿using LogAnalyser.Domain.Interfaces.Parser;
using System.Linq;
using LogAnalyser.Domain.Entities;
using System.Text.RegularExpressions;

namespace LogAnalyser.Domain.Parsers
{
    public class LogWebhookParser : ILogWebhookParser
    {
        private Regex rgx = new Regex(@"(((request_to=)\""(http|https):\/\/(www\.){0,1}\w*\.(([a-z]{2,3})|([a-z]{3}\.[a-z]{2}))\""))|((response_status=)\""[1-5]{1}\d{2}\"")");

        public LogWebhook Parse(string stringToParse)
        {
            var matches = Validate(stringToParse);
            if (matches.Count != 2)
                return null;

            return DoParse(matches);
        }

        private LogWebhook DoParse(MatchCollection matches)
        {
            var logWebhook = new LogWebhook();
            foreach (Match match in matches)
            {
                CreateLogWebhookByMatch(match.Value, ref logWebhook);
            }

            return logWebhook;
        }

        private MatchCollection Validate(string stringToParse)
        {
           return rgx.Matches(stringToParse);
        }

        private void CreateLogWebhookByMatch(string match, ref LogWebhook logWebhook)
        {
            var arrMatch = match.Split('=');
            switch (arrMatch.ElementAt(0))
            {
                case "request_to":
                    logWebhook.RequestToUrl = arrMatch.ElementAt(1).Replace("\"", "");
                    break;

                case "response_status":
                    logWebhook.ResponseStatusCode = arrMatch.ElementAt(1).Replace("\"", ""); ;
                    break;
            }
        }
    }
}
