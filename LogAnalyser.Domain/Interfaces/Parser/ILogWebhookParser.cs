﻿using LogAnalyser.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyser.Domain.Interfaces.Parser
{
    public interface ILogWebhookParser
    {
        LogWebhook Parse(string stringToParse);
    }
}
