﻿using LogAnalyser.Domain.Entities;
using System.Collections.Generic;

namespace LogAnalyser.Domain.Interfaces.Strategy
{
    public interface IAnalyserStrategy
    {
        IEnumerable<LogWebhookReport> GetTopUrl(IEnumerable<LogWebhook> logList, int top);
        IEnumerable<LogWebhookReport> GetTopStatusCode(IEnumerable<LogWebhook> logList);
    }
}
