﻿using LogAnalyser.Domain.Entities;
using System.Collections.Generic;

namespace LogAnalyser.Domain.Interfaces.Repository
{
    public interface ILogWebhookRepository
    {
        IList<LogWebhook> Load(string filePath);
    }
}
