﻿using LogAnalyser.Domain.Parsers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogAnalyser.Domain.Test.Parsers
{
    [TestClass]
    public class LogWebhookParserTest
    {
        private string correctLogLine = "";
        private LogWebhookParser mockParser;

        [TestInitialize]
        public void Initialize()
        {
            mockParser = new LogWebhookParser();
            correctLogLine = @"level = info response_body ="" request_to=""https://easterncobra.com.br"" response_headers=map[Keep-Alive:[timeout=20] Cache-Control:[max-age=0, private, must-revalidate] Status:[200] Etag:[7215ee9c7d9dc229d2921a40e899ec5f] Vary:[Accept-Encoding] X-Ua-Compatible:[IE=Edge,chrome=1] Server:[nginx] X-Request-Id:[1381e8cb388db085cdc3bac457dab9bf] Date:[Tue, 07 Jul 2015 18:29:52 GMT] Content-Type:[text/html; charset=utf-8] X-Powered-By:[Phusion Passenger (mod_rails/mod_rack) 3.0.17] X-Rack-Cache:[invalidate, pass] X-Runtime:[0.034645] Connection:[keep-alive] Set-Cookie:[X-Mapping-fjhppofk=A67D55AC8119CAD031E35EC35B4BCFFD; path=/]] response_status=""400""";
        }

        [TestMethod]
        public void ShouldReturnLogWebhookInstance()
        {
            Assert.IsNotNull(mockParser.Parse(correctLogLine));
        }

        [TestMethod]
        public void ShouldReturnCurrectUrl()
        {
            var logWebhook = mockParser.Parse(correctLogLine);
            Assert.IsTrue(logWebhook.RequestToUrl == "https://easterncobra.com.br");
        }

        [TestMethod]
        public void ShouldReturnCurrectStatusCode()
        {
            var logWebhook = mockParser.Parse(correctLogLine);
            Assert.IsTrue(logWebhook.ResponseStatusCode == "400");
        }

        [TestMethod]
        public void ShouldReturnNullToLogWithIncorrectUrl()
        {
            var _logLine = @"level = info response_body ="" request_to=""htt://easterncobra.com.br"" response_headers=map[Keep-Alive:[timeout=20] Cache-Control:[max-age=0, private, must-revalidate] Status:[200] Etag:[7215ee9c7d9dc229d2921a40e899ec5f] Vary:[Accept-Encoding] X-Ua-Compatible:[IE=Edge,chrome=1] Server:[nginx] X-Request-Id:[1381e8cb388db085cdc3bac457dab9bf] Date:[Tue, 07 Jul 2015 18:29:52 GMT] Content-Type:[text/html; charset=utf-8] X-Powered-By:[Phusion Passenger (mod_rails/mod_rack) 3.0.17] X-Rack-Cache:[invalidate, pass] X-Runtime:[0.034645] Connection:[keep-alive] Set-Cookie:[X-Mapping-fjhppofk=A67D55AC8119CAD031E35EC35B4BCFFD; path=/]] response_status=""400""";
            Assert.IsNull(mockParser.Parse(_logLine));
        }

        [TestMethod]
        public void ShouldReturnNullToLogWithoutStatusCode()
        {
            var _logLine = @"level = info response_body ="" request_to=""htt://easterncobra.com.br"" response_headers=map[Keep-Alive:[timeout=20] Cache-Control:[max-age=0, private, must-revalidate] Status:[200] Etag:[7215ee9c7d9dc229d2921a40e899ec5f] Vary:[Accept-Encoding] X-Ua-Compatible:[IE=Edge,chrome=1] Server:[nginx] X-Request-Id:[1381e8cb388db085cdc3bac457dab9bf] Date:[Tue, 07 Jul 2015 18:29:52 GMT] Content-Type:[text/html; charset=utf-8] X-Powered-By:[Phusion Passenger (mod_rails/mod_rack) 3.0.17] X-Rack-Cache:[invalidate, pass] X-Runtime:[0.034645] Connection:[keep-alive] Set-Cookie:[X-Mapping-fjhppofk=A67D55AC8119CAD031E35EC35B4BCFFD; path=/]]";
            Assert.IsNull(mockParser.Parse(_logLine));
        }

        [TestMethod]
        public void ShouldReturnNullToLogWithStatusCode600()
        {
            var _logLine = @"level = info response_body ="" request_to=""htt://easterncobra.com.br"" response_headers=map[Keep-Alive:[timeout=20] Cache-Control:[max-age=0, private, must-revalidate] Status:[200] Etag:[7215ee9c7d9dc229d2921a40e899ec5f] Vary:[Accept-Encoding] X-Ua-Compatible:[IE=Edge,chrome=1] Server:[nginx] X-Request-Id:[1381e8cb388db085cdc3bac457dab9bf] Date:[Tue, 07 Jul 2015 18:29:52 GMT] Content-Type:[text/html; charset=utf-8] X-Powered-By:[Phusion Passenger (mod_rails/mod_rack) 3.0.17] X-Rack-Cache:[invalidate, pass] X-Runtime:[0.034645] Connection:[keep-alive] Set-Cookie:[X-Mapping-fjhppofk=A67D55AC8119CAD031E35EC35B4BCFFD; path=/]] response_status=""600""";
            Assert.IsNull(mockParser.Parse(_logLine));
        }
    }
}
