﻿using LogAnalyser.Domain.Entities;
using LogAnalyser.Domain.Strategies;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace LogAnalyser.Domain.Test.Strategies
{
    [TestClass]
    public class AnalyserStrategyTest
    {
        private AnalyserStrategy mockAnalyserStrategy;
        private IEnumerable<LogWebhook> mockLogWebhookList;

        [TestInitialize]
        public void Initialize()
        {
            mockAnalyserStrategy = new AnalyserStrategy();
            mockLogWebhookList = LoadMockLogWebhookList();
        }

        [TestMethod]
        public void ShouldReturnCurrectCountItensInReportResult()
        {
            var topCountResult = 3;
            var reportList = mockAnalyserStrategy.GetTopUrl(mockLogWebhookList, topCountResult);
            Assert.IsTrue(reportList.Count() == topCountResult);
        }

        [TestMethod]
        public void ShouldReturnCurrectUrlInFirstElement()
        {
            var reportList = mockAnalyserStrategy.GetTopUrl(mockLogWebhookList, 3);
            Assert.IsTrue(reportList.First().LogInfo == "https://easterncobra.com.br");
        }

        [TestMethod]
        public void ShouldReturnCurrectStatusCodeInFirstElement()
        {
            var reportList = mockAnalyserStrategy.GetTopStatusCode(mockLogWebhookList);
            Assert.IsTrue(reportList.First().LogInfo == "200");
        }

        [TestMethod]
        public void ShouldReturnCurrectTotalCountToFirstUrl()
        {
            var reportList = mockAnalyserStrategy.GetTopUrl(mockLogWebhookList, 2);
            Assert.IsTrue(reportList.First().TotalCount == 7);
        }

        [TestMethod]
        public void ShouldReturnCurrectTotalCountToFirstStatusCode()
        {
            var reportList = mockAnalyserStrategy.GetTopStatusCode(mockLogWebhookList);
            Assert.IsTrue(reportList.First().TotalCount == 4);
        }

        private IEnumerable<LogWebhook> LoadMockLogWebhookList()
        {
            var _mockLogWebhookList = new List<LogWebhook>();
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://abandonedpluto.com", ResponseStatusCode = "200" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://abandonedpluto.com", ResponseStatusCode = "300" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://abandonedpluto.com", ResponseStatusCode = "400" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://abandonedpluto.com", ResponseStatusCode = "500" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://abandonedpluto.com", ResponseStatusCode = "501" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://abandonedpluto.com", ResponseStatusCode = "502" });           

            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://notoriouslonesome.com", ResponseStatusCode = "200" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://notoriouslonesome.com", ResponseStatusCode = "200" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://notoriouslonesome.com", ResponseStatusCode = "200" });

            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://easterncobra.com.br", ResponseStatusCode = "404" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://easterncobra.com.br", ResponseStatusCode = "403" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://easterncobra.com.br", ResponseStatusCode = "203" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://easterncobra.com.br", ResponseStatusCode = "500" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://easterncobra.com.br", ResponseStatusCode = "404" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://easterncobra.com.br", ResponseStatusCode = "203" });
            _mockLogWebhookList.Add(new LogWebhook() { RequestToUrl = "https://easterncobra.com.br", ResponseStatusCode = "300" });

            return _mockLogWebhookList;
        }
    }
}
