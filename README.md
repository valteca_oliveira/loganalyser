# Desafio - Análise de Log#

Este projeto consiste em uma aplicação Console para análise e report das informações conforme especificado no desafio.

A aplicação foi desenvolvida em .NET (C#) e abaixo seguem os passos para execução:

### Execução Aplicação (Console) ###

1. Após abertura da aplicação no VisualStudio, setar o projeto "**LogAnalyser.Console**" como "Start Project" (para setar esta opção basta clicar com o botão direito sobre o respectivo projeto e em seguida selecionar a opção "Set as Start Project");


2. Ajustar o path/nome do arquivo de log (para ajustar estas configurações basta alterar a variável de configuração "**LOG_FILE_PATH**", contida no arquivo "**App.config**" do projeto Console);


3. Executar a aplicação;

### Execução dos Testes ###

1. Abrir a aba "Test Explorer";
2. Clicar sobre a opção "Run All";