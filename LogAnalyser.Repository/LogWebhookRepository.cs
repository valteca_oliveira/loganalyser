﻿using LogAnalyser.Domain.Entities;
using LogAnalyser.Domain.Interfaces.Parser;
using LogAnalyser.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.IO;

namespace LogAnalyser.Repository
{
    public class LogWebhookRepository : ILogWebhookRepository
    {
        private ILogWebhookParser _parser;        

        public LogWebhookRepository(ILogWebhookParser parser)
        {
            _parser = parser;           
        }        

        public IList<LogWebhook> Load(string filePath)
        {
            if (File.Exists(filePath) == false)
                throw new FileNotFoundException();

            IList<LogWebhook> logList = null;
            using (var fileStream = new StreamReader(filePath))
            {
                if (fileStream.BaseStream.Length == 0)
                    throw new Exception("O arquivo de log está vazio");

                logList = new List<LogWebhook>();
                while (!fileStream.EndOfStream)
                {
                    var line = fileStream.ReadLine();
                    var log = _parser.Parse(line.Trim());

                    if (log != null)
                        logList.Add(log);
                }
            }

            return logList;
        }
    }
}
