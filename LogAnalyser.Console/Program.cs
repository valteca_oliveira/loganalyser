﻿using LogAnalyser.Console.Factories;
using LogAnalyser.Domain.Utils;

namespace LogAnalyser.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            string logFilePath = ConfigurationWrapper.GetConfiguration("LOG_FILE_PATH");
            var analyser = AnalyserFactory.GetAnalyser();
            System.Console.WriteLine(analyser.GetReport(logFilePath));
            System.Console.ReadKey();
        }       
    }
}
