﻿using LogAnalyser.Domain;
using LogAnalyser.Domain.Parsers;
using LogAnalyser.Domain.Strategies;
using LogAnalyser.Repository;

namespace LogAnalyser.Console.Factories
{
    public static class AnalyserFactory
    {
        public static Analyser GetAnalyser()
        {
            var _parser = new LogWebhookParser();
            var _repository = new LogWebhookRepository(_parser);
            var _analyserStrategy = new AnalyserStrategy();
            return new Analyser(_repository, _analyserStrategy);
        }
    }
}
